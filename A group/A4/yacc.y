%{
#include<stdio.h>
#include<string.h>
#include "y.tab.h"
extern FILE *yyin;
%}

%token MAIN PRINTF IF ELSE FOR WHILE RETURN INTEGER FLOAT STCONST LOGOP OP DATATYPE IDENTIFIER

%%
main: MAIN '('')' '{' body '}' {printf("Main Encountered\n");}
;
body: varstmt stmtlist
;
varstmt:varlist varstmt |
;
varlist:DATATYPE variable ';'{printf("Valid declaration\n");}
;
variable: IDENTIFIER ',' variable | IDENTIFIER
;
stmtlist:stmt stmtlist |
;
stmt: printf
printf: PRINTF '(' STCONST ')' ';' {printf("Print statement encountered\n");}
;
%%

main()
{
	yyin = fopen("input.txt","r");
	yyparse();
}

void yyerror(const char *s)
{
	printf("Invalid character %s\n", s);
}