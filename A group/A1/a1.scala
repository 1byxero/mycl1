object a1{

	def main(args:Array[String]){
		println("Enter elements of the array")		

		
		val l = Console.readLine		
		var arr:Array[Int] = l.split(" ").map(_.toInt)
		
		
		for(i<- 0 to arr.length-1){
			for(j<- i to arr.length-1)
				if(arr(i)>arr(j)){
					val temp = arr(j)
					arr(j) = arr(i)
					arr(i) = temp
				}
		}

		for(i<- 0 to arr.length-1)
			println(arr(i))

		println("Enter element to search from the array")
		val search:Int = Console.readInt

		val start = 0
		val end = arr.length-1

		val index:Int = binarysearch(arr,start,end,search)
		if(index>=0)
			println("Element found at", index)
		else
			println("Element not found")
	}

	def binarysearch(list:Array[Int],start:Int,end:Int,search:Int):Int={		
		val mid:Int = start + ((end-start)/2)
		println(start,end,mid)

		if(start<end){		
			if(list(mid)==search)
				return mid
			else if(mid<search)
				return binarysearch(list,mid+1,end,search)
			else
				return binarysearch(list,start,mid-1,search)		
		}		
		else
			return -1
	}
}