%{
#include <stdio.h>
#include <string.h>
FILE *opfile;
int lineno = 0;
int symbolcount = 0;
char symboltable[20][20];
int errorcount = 0;
char errordescription[20][200];
int errorline[20];
%}

%%
printf|scanf {printf("%d\t\t%s\t\tLibrary Function\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
[0-9]+ {printf("%d\t\t%s\t\tNumber\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
[-+*/] {printf("%d\t\t%s\t\tOperator\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
= {printf("%d\t\t%s\t\tAssignment\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
include|main|return {printf("%d\t\t%s\t\tKeyword\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
int|float|char|double {printf("%d\t\t%s\t\tData-type\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
[\t] ;
\n {lineno++; fprintf(opfile,"\n");}
, {printf("%d\t\t%s\t\tSeparaotr\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
# {printf("%d\t\t%s\t\tPreporcessor\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
; {printf("%d\t\t%s\t\tTerminator\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\{ {printf("%d\t\t%s\t\tStart of Block\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\} {printf("%d\t\t%s\t\tEnd of Block\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\[ {printf("%d\t\t%s\t\tOpen Sqaure Bracket\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\] {printf("%d\t\t%s\t\tClose Sqaure Bracket\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\( {printf("%d\t\t%s\t\tOpen Round Bracket\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\) {printf("%d\t\t%s\t\tClose Round Bracket\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\< {printf("%d\t\t%s\t\tLess than\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
\> {printf("%d\t\t%s\t\tGreater than\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
(\"[^\"]*\") {printf("%d\t\t%s\t\tString Constant\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
(\"[^\"\n]*\n) {errorline[errorcount]=lineno; char str[100] = "Unterminated Quotes "; strcat(str,yytext); strcpy(errordescription[errorcount],str); errorcount++; lineno++;}
[a-zA-Z][a-zA-Z0-9]* {printf("%d\t\t%s\t\tIdentifier\n",lineno,yytext); fprintf(opfile,"%s ",yytext); add_symbol(yytext);}
(\/\/.*) ;
(\/\*[^*/]*\*\/) ; 
(\/\*[^*/]*) {errorline[errorcount]=lineno; char str[100]="Unterminated comment "; strcat(str,yytext); strcpy(errordescription[errorcount],str); errorcount++;}
([a-zA-Z0-9]+\.h) {printf("%d\t\t%s\t\tHeader file\n",lineno,yytext); fprintf(opfile,"%s ",yytext);}
[0-9]+[a-zA-Z]+ {errorline[errorcount]=lineno; char str[100]="Unrecognized token "; strcat(str,yytext); strcpy(errordescription[errorcount],str); errorcount++;}
[a-zA-Z][a-zA-Z0-9]+[\$@$][a-zA-Z0-9]* {errorline[errorcount]=lineno; char str[100]="Unrecognized token "; strcat(str,yytext); strcpy(errordescription[errorcount],str); errorcount++;}
%%

//function to add symbol in symbol table
void add_symbol(char s[10])
{
int i;
for(i=0;i<symbolcount;i++)
	{
	if(strcmp(symboltable[i],s)==0)
		return;	
	}
strcpy(symboltable[symbolcount],s);
symbolcount++;
}

//main
int main()
{
char inputfile[20],outputfile[20];

printf("\nEnter input file name:		");
scanf("%s",&inputfile);

printf("\nEnter output file name:		");
scanf("%s",&outputfile);

yyin = fopen(inputfile,"r");
opfile = fopen(outputfile,"w");

int i;

printf("\nLine No\t\tLEXEME\t\tToken\n");
yylex(); //this will parse file and show toeksn ands lexemes

//show symbol table
printf("Symbol Table\n");
for(i=0;i<symbolcount;i++)
{
	printf("%s\n",symboltable[i]);
}

//show errors
printf("Lexical errors in the file are\n");
for(i=0;i<errorcount;i++)
{
	printf("Line no: %d \tError:%s\n",errorline[i],errordescription[i]);
}
return 0;
}

int yywrap()
{
return 1;
}
