#include<iostream>
#include<pthread.h>
using namespace std;

struct array
	{
		int *arr;
		int first;
		int last;
	};

int size;

void *quick(void *in)
	{

		array* a = (array*)in;	
		int i,j;	

		pthread_t id= pthread_self();

		if(a->first<a->last)
			{
				int temp;
				int i = a->first;
				int j = a->last;				
				int pivot = a->arr[a->first];
				while(i<j)
					{
						while(a->arr[i]<=pivot && i<j)
							i++;
						while(a->arr[j]>pivot && i<=j)
							j--;

						if(i<=j)
						{
							temp = a->arr[i];
							a->arr[i] = a->arr[j];
							a->arr[j] = temp;
						}
					}

				temp = a->arr[j];
				a->arr[j] = pivot;
				a->arr[a->first] = temp;


				pthread_t threads[2];

				cout<<"Thread ID: "<<id<<", Pivot for this thread is "<<pivot<<endl;
				array a1,a2;

				a1.arr = new int[size];
				a2.arr = new int[size];

				a1.arr = a->arr;
				a2.arr = a->arr;

				a1.first = a->first;
				a1.last = j-1;
				a2.first = j+1;
				a2.last = a->last;

				pthread_create(&threads[0],NULL,&quick,(void *)&a1); 
				//here a thread is created with 'a1' as parameter and not 'a' which is our main array
				//a1 has first =0 and last =j-1
				pthread_create(&threads[1],NULL,&quick,(void *)&a2);
				//here a thread is created with 'a2' as parameter and not 'a' which is our main array
				//a2 has first =j+1 and last =end
				//so in the recuresion smaller and smaller arrays are quicksorted




				pthread_join(threads[0],NULL);
				pthread_join(threads[1],NULL);
				//this basically executes them sequentially.. not exactly sequenctially but u is equivalent to sequential check pthread join online
			}
	}


int main()	
	{
		array a1;		
		cout<<"Enter the size of the array\n";
		cin>>size;

		a1.arr = new int[size];
		a1.first = 0;
		a1.last = size - 1;				

		cout<<"Enter the elements of the array\n";

		for(int i=0;i<size;i++)
			{
				cin>>a1.arr[i];
			}

		quick(&a1);

		cout<<"Sorted array is\n";
		for(int i=0;i<size;i++)
			cout<<a1.arr[i]<<" ";
		cout<<"\n";

		return 0;
	}