%{
#include <stdio.h>
#include <string.h>
#include<stdlib.h>   

struct symboltable
{
	char name[10];
	char type[10];
	double value;
}sym[10];

struct quadruple
{
	char result[10];
	char operator[5];
	char operand1[10];
	char operand2[10];
}quad[25];

struct stack
{
	char *item[10];
	int top;	
}stk;


int search_sym(char []);
void add_symbol(char [],char [],double);
void display_sym();
void add_quad(char [],char [],char [],char []);
void display_quad();
void push(char*);
char* pop();

int quad_count=0;
int temp_var=0;
int sym_count=0;

%}

%union
{
	int ival;
	double dval;
	char string[10];
}

%token <dval> NUMBER
%token <string> TYPE
%token <string> ID
%type <string> varlist
%type <string> expr
%token MAIN
%left '+' '-'
%left '*' '/'

%%
program: MAIN '(' ')' '{' body '}'
;
body: varstmt stmtlist
;
varstmt: vardecl varstmt |
;
vardecl: TYPE varlist ';' 
;
varlist: varlist ',' ID {
				
				int i;
				i = search_sym($3);
				if(i!=-1)
					printf("Multiple declarations of variables\n");
				else
					add_symbol($3,$<string>0,0);
							
							
}
|
ID '=' NUMBER {
				int i;
				i = search_sym($1);
				if(i!=-1)
					printf("Multiple declarations of variables\n");
				else
					add_symbol($1,$<string>0,$3);	
}
|
varlist ',' ID '=' NUMBER {
				int i;
				i = search_sym($3);
				if(i!=-1)
					printf("Multiple declarations of variables\n");
				else
					add_symbol($3,$<string>0,$5);					
}
|
ID {
				int i;
				i = search_sym($1);
				if(i!=-1)
					printf("Multiple declarations of variables\n");
				else
					add_symbol($1,$<string>0,0);
}
;
stmtlist: stmt stmtlist|
;
stmt: ID '=' NUMBER {
				char temp[10];
				int i;
				i = search_sym($1);
				if(i==-1)
					printf("Undefined variable");
				else
					{
						char temp[100];
						if(strcmp(sym[i].type,"int")==0)
							sprintf(temp,"%d",(int)$3);
						else
							snprintf(temp,10,"%f",$3);
					}
				add_quad("=","",temp,$1);
}
|
ID '=' ID ';' {
				int i,j;
				i = search_sym($1);
				j = search_sym($3);
				if(i==-1 || j==-1)
					printf("Undefined variable");
				else
					add_quad("=","",$3,$1);	
}
|
ID '=' expr ';' {add_quad("=","",pop(),$1);}
;

expr: expr '+' expr {
				char str[5],str1[5] = "t";
				sprintf(str, "%d", temp_var);
				strcat(str1,str);
				temp_var++;
				add_quad("+",pop(),pop(),str1);
				push(str1);
}
|
expr '-' expr {
				char str[5],str1[5] = "t";
				sprintf(str, "%d", temp_var);
				strcat(str1,str);
				temp_var++;
				add_quad("-",pop(),pop(),str1);
				push(str1);
	
}
|
expr '*' expr {
				char str[5],str1[5] = "t";
				sprintf(str, "%d", temp_var);
				strcat(str1,str);
				temp_var++;
				add_quad("*",pop(),pop(),str1);
				push(str1);
}
|
expr '/' expr {
				char str[5],str1[5] = "t";
				sprintf(str, "%d", temp_var);
				strcat(str1,str);
				temp_var++;
				add_quad("/",pop(),pop(),str1);
				push(str1);
}
| 
ID {
				int i;
				i = search_sym($1);
				if(i==-1)
					printf("Undefined variable\n");
				else
					push($1);					
}
|
NUMBER {
				char temp_str[10];
				snprintf(temp_str,10,"%f",$1);
				push(temp_str);
}
;
%%
extern FILE *yyin;
int main()
{
	stk.top = -1;
	yyin = fopen("input.txt","r");
	yyparse();
	display_sym();
	printf("\n\n");
	display_quad();
	return 0;
}

int search_sym(char sym_name[10])
{
	int i;
	int flag=0;
	for(i=0;i<sym_count;i++)
	{
		if(strcmp(sym[i].name,sym_name)==0)
			flag=1;
	}		
	if(flag)
		return i;
	else
		return -1;
}


void add_symbol(char name[10],char type[10],double value)
{
	strcpy(sym[sym_count].name,name);
	strcpy(sym[sym_count].type,type);
	sym[sym_count].value = value;
	sym_count++;
}

void display_sym()
{
	int i;
	printf("Symbol Table\n");
	printf("Name\t\tType\t\tValue\n");
	for(i=0;i<sym_count;i++)
	{
		printf("%s\t\t%s\t\t%f\n",sym[i].name,sym[i].type,sym[i].value);
	}
}

void add_quad(char operator[10],char operand2[10],char operand1[],char result[10])
{	
	strcpy(quad[quad_count].operator,operator);
	strcpy(quad[quad_count].operand1,operand1);
	strcpy(quad[quad_count].operand2,operand2);
	strcpy(quad[quad_count].result,result);
	quad_count++;
	
}
void display_quad()
{
	printf("Quadraple Table\n");
	printf("Result\t\tOperator\t\tOperand1\t\tOperand2\n");
	int i;
	for(i=0;i<quad_count;i++)	
		printf("%s\t\t%s\t\t%s\t\t%s\n",quad[i].result,quad[i].operator,quad[i].operand1,quad[i].operand2);
}
void push(char* str)
{
	stk.top++;
	stk.item[stk.top]= (char *)malloc(strlen(str)+1);
	strcpy(stk.item[stk.top],str);
}
char* pop()
{
	if(stk.top==-1)
		{
		printf("Stack Empty\n");
		exit(0);
		}
	
	char *str = (char *)malloc(strlen(stk.item[stk.top])+1);
	strcpy(str,stk.item[stk.top]);
	stk.top--;	
	return str;
}

int yyerror(){
	return 1;
}