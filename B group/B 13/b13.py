import csv,random
from math import sqrt
from collections import Counter
from operator import itemgetter

def loaddata():
	f = open("iris.data",'r')
	lines = csv.reader(f)
	dataset = list(lines)

	random.shuffle(dataset)

	train = dataset[:15]
	test = dataset[15:]

	return train,test
	
def calculate(train,test):
	for testitem in test:
		distaces=[]
		topk_y=[]

		for trainitem in train:
			dist = pow((int(trainitem[0])-int(testitem[0])),2)
			distaces.append([sqrt(dist),trainitem])

		sorted_distances = sorted(distaces,key=itemgetter(0))

		topk = sorted_distances[:3]

		for (a,b) in topk:
			topk_y.append(b)

		classes = Counter(q for (p,q) in topk_y)

		print("Class for "+str(testitem)+" is "+max(classes))



def main():
	train,test = loaddata()
	calculate(train,test)

	

main()